include( FindPackageHandleStandardArgs )
include( VistaFindUtils )

if( NOT VSKETCHUP_FOUND )

	# Try to find new version > 2013
	# Skip LayOut

	vista_find_package_root( SKETCHUP "headers/SketchUpAPI/slapi.h" )	

	if( SKETCHUP_ROOT_DIR )
		set( SKETCHUP_INCLUDE_DIRS "${SKETCHUP_ROOT_DIR}/headers" )
		if( VISTA_64BIT )
			set( SKETCHUP_LIBRARY_DIRS "${SKETCHUP_ROOT_DIR}/binaries/sketchup/x64" )
			set( SKETCHUP_LIBRARIES SketchUpAPI sketchup )
		else( )
			set( SKETCHUP_LIBRARY_DIRS "${SKETCHUP_ROOT_DIR}/binaries/sketchup/x64" )
			set( SKETCHUP_LIBRARIES sketchup )
		endif( )
	
	else( )
		
		# Try to find old version
		
		vista_find_package_root( SKETCHUP "include/slapi/slapi.h" )	

		if( SKETCHUP_ROOT_DIR )
			set( SKETCHUP_INCLUDE_DIRS "${SKETCHUP_ROOT_DIR}/include" )
			set( SKETCHUP_LIBRARIES "slapi" "sketchup" )
			set( SKETCHUP_LIBRARY_DIRS "${SKETCHUP_ROOT_DIR}/lib" "${SKETCHUP_ROOT_DIR}/bin" )
		else( )
			message( WARNING "vista_find_package_root - File named slapi.h not found" )	
		endif( )
		
	endif( )
	
endif( )

find_package_handle_standard_args( VSketchUp "SketchUp API could not be found" SKETCHUP_ROOT_DIR )
