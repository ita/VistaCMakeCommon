include( FindPackageHandleStandardArgs )
include( VistaFindUtils )

if( NOT VPOCO_FOUND )
	vista_find_package_root( POCO "include/POCO/Poco.h" NAMES poco )	

	if( POCO_ROOT_DIR )
		set( POCO_INCLUDE_DIRS "${POCO_ROOT_DIR}/include" )		
		set( POCO_LIBRARY_DIRS "${POCO_ROOT_DIR}/lib" "${POCO_ROOT_DIR}/bin" )
		# poco will include libs by pragma comment in headers
		set( POCO_LIBRARIES optimized PocoFoundation PocoNet debug PocoFoundationd PocoNetd )
	endif( )
endif( )

find_package_handle_standard_args( VPOCO "POCO could not be found" POCO_ROOT_DIR )
