# $Id: FindVOPENVR.cmake 31553 2012-08-13 13:21:14Z dr165799 $

include( FindPackageHandleStandardArgs )
include( VistaFindUtils )

if( NOT VOPENVR_FOUND )
	vista_find_package_root( OpenVR headers/openvr.h )

	if( OPENVR_ROOT_DIR )		
		set( OPENVR_INCLUDE_DIRS "${OPENVR_ROOT_DIR}/headers" )
		if( WIN32 )
			if( VISTA_64BIT )
				set( OPENVR_LIBRARY_DIRS "${OPENVR_ROOT_DIR}/lib/win64" "${OPENVR_ROOT_DIR}/bin/win64" )
			else()
				set( OPENVR_LIBRARY_DIRS "${OPENVR_ROOT_DIR}/lib/win32" "${OPENVR_ROOT_DIR}/bin/win32" )
			endif()
			set( OPENVR_LIBRARIES openvr_api )
		elseif( UNIX )
			if( VISTA_64BIT )
				set( OPENVR_LIBRARY_DIRS "${OPENVR_ROOT_DIR}/lib/linux64" "${OPENVR_ROOT_DIR}/bin/linux64" )
			else()
				set( OPENVR_LIBRARY_DIRS "${OPENVR_ROOT_DIR}/lib/linux32" "${OPENVR_ROOT_DIR}/bin/linux32" )
			endif()
			set( OPENVR_LIBRARIES openvr_api )
		endif()
	endif()
endif()

find_package_handle_standard_args( VOPENVR "OpenVR could not be found" OPENVR_ROOT_DIR OPENVR_LIBRARIES )

