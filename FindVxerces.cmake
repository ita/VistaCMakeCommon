include( FindPackageHandleStandardArgs )
include( VistaFindUtils )

if( NOT VXERCES_FOUND )
	vista_find_package_root( XERCES "src/xercesc/parsers/XercesDOMParser.hpp" NAMES Xerces xserces-c )	

	if( XERCES_ROOT_DIR )
	
		set( XERCES_INCLUDE_DIRS "${XERCES_ROOT_DIR}/src" )
		
		if( WIN32 )
		
			if( VISTA_64BIT )
			
				set( XERCES_LIBRARY_DIRS "${XERCES_ROOT_DIR}/Build/Win64/VC12/Debug" )
				list( APPEND XERCES_LIBRARY_DIRS "${XERCES_ROOT_DIR}/Build/Win64/VC12/Release" )
				list( APPEND XERCES_LIBRARY_DIRS "${XERCES_ROOT_DIR}/Build/Win64/VC12/Static Debug" )
				list( APPEND XERCES_LIBRARY_DIRS "${XERCES_ROOT_DIR}/Build/Win64/VC12/Static Release" )
				set( XERCES_LIBRARIES optimized xerces-c_3 debug xerces-c_3D )
				#set( XERCES_LIBRARIES optimized xerces-c_static_3 debug xerces-c_static_3D )
				
			else( )
			
				set( XERCES_LIBRARY_DIRS "${XERCES_ROOT_DIR}/Build/Win32/VC12/Debug" )
				list( APPEND XERCES_LIBRARY_DIRS "${XERCES_ROOT_DIR}/Build/Win32/VC12/Release" )
				list( APPEND XERCES_LIBRARY_DIRS "${XERCES_ROOT_DIR}/Build/Win32/VC12/Static Debug" )
				list( APPEND XERCES_LIBRARY_DIRS "${XERCES_ROOT_DIR}/Build/Win32/VC12/Static Release" )
				set( XERCES_LIBRARIES optimized xerces-c_3 debug xerces-c_3D )
				#set( XERCES_LIBRARIES optimized xerces-c_static_3 debug xerces-c_static_3D )
				
			endif( )
			
		else( )
			set( XERCES_LIBRARY_DIRS "${XERCES_ROOT_DIR}/lib" "${XERCES_ROOT_DIR}/bin" )
			set( XERCES_LIBRARIES XERCES )
		endif( )
		
	else( XERCES_ROOT_DIR )
	
		message( WARNING "vista_find_package_root - File named parsersXercesDOMParser.hpp not found" )	

	endif( XERCES_ROOT_DIR )
	
endif( NOT VXERCES_FOUND )

find_package_handle_standard_args( VXERCES "XERCES could not be found" XERCES_ROOT_DIR )
