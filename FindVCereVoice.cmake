

include( FindPackageHandleStandardArgs )
include( VistaFindUtils )

if( NOT VCEREVOICE_FOUND )
	vista_find_package_root( CereVoice /cerevoice/lib64/libcerevoice_shared-4.dll NAMES cerevoice )
    
	if( CEREVOICE_ROOT_DIR )
		if( UNIX )
			message( FATAL_ERROR "The FindVCereVoice.cmake script is not yet implemented for Linux, if you want to use it do so!" )
		else()
			set( CEREVOICE_INCLUDE_DIRS 
				${CEREVOICE_ROOT_DIR}/3rdparty/include
				${CEREVOICE_ROOT_DIR}/cerevoice_eng/include
				${CEREVOICE_ROOT_DIR}/cerevoice_aud/include
			)
			
			set( CEREVOICE_LIBRARY_DIRS
					${CEREVOICE_ROOT_DIR}/3rdparty/lib64
					${CEREVOICE_ROOT_DIR}/cerevoice/lib64
					${CEREVOICE_ROOT_DIR}/cerevoice_eng/lib64
					${CEREVOICE_ROOT_DIR}/cerevoice_pmod/lib64
					${CEREVOICE_ROOT_DIR}/cerevoice_aud/lib64
					${CEREVOICE_ROOT_DIR}/cerehts/lib64
				   )       
			
			set( CEREVOICE_LIBRARIES            
				optimized libcerevoice_shared-4
				debug libcerevoice_shared-4
				optimized libcerevoice_eng_shared-4
				debug libcerevoice_eng_shared-4 
				optimized libcerevoice_aud_shared-4
				debug libcerevoice_aud_shared-4
				optimized libcerevoice_pmod_shared-4
				debug libcerevoice_pmod_shared-4
				optimized libcerehts_shared-4
				debug libcerehts_shared-4
			)
		endif()
		
		#set the voices path
		add_definitions(-DCEREVOICE_VOICES_PATH="${CEREVOICE_ROOT_DIR}/../voices/")
     		
    endif()
endif( NOT VCEREVOICE_FOUND )

find_package_handle_standard_args( VCereVoice "CereVoice could not be found" CEREVOICE_ROOT_DIR )
