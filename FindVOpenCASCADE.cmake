include( FindPackageHandleStandardArgs )
include( VistaFindUtils )

if( NOT VOPENCASCADE_FOUND )
    vista_find_package_root( OpenCASCADE inc/OSD.hxx )
    if( OPENCASCADE_ROOT_DIR )
        set( OPENCASCADE_INCLUDE_DIRS "${OPENCASCADE_ROOT_DIR}/inc" )
        set( OPENCASCADE_LIBRARY_DIRS "${OPENCASCADE_ROOT_DIR}/win64/vc12/lib" "${OPENCASCADE_ROOT_DIR}/win64/vc12/libd" "${OPENCASCADE_ROOT_DIR}/win64/vc12/lib" "${OPENCASCADE_ROOT_DIR}/win64/vc12/bind" )
		set( OPENCASCADE_LIBRARIES TKernel TKMath TKBRep TKGeomBase TKGeomAlgo TKG3d TKG2d TKShHealing TKTopAlgo TKMesh TKPrim TKBool TKBO TKFillet TKSTEP TKSTEPBase TKSTEPAttr TKXSBase TKSTEP209 TKIGES TKOffset )
    endif( OPENCASCADE_ROOT_DIR )
endif( NOT VOPENCASCADE_FOUND )

find_package_handle_standard_args( VOpenCASCADE "OpenCASCADE could not be found" OPENCASCADE_ROOT_DIR )