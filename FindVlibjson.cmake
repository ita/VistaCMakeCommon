include( FindPackageHandleStandardArgs )
include( VistaFindUtils )

if( NOT VLIBJSON_FOUND )
	vista_find_package_root( libjson "include/libjson.h" NAMES libjson )	

	if( LIBJSON_ROOT_DIR )
		set( LIBJSON_INCLUDE_DIRS "${LIBJSON_ROOT_DIR}/include" )		
		set( LIBJSON_LIBRARY_DIRS "${LIBJSON_ROOT_DIR}/lib" "${LIBJSON_ROOT_DIR}/bin" )	
		set( LIBJSON_LIBRARIES optimized libjson debug libjsonD )
	endif( )
endif( )

find_package_handle_standard_args( VLIBJSON "libjson could not be found" LIBJSON_ROOT_DIR )
