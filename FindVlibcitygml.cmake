include( FindPackageHandleStandardArgs )
include( VistaFindUtils )

if( NOT VLIBCITYGML_FOUND )

	vista_find_package_root( libcitygml "include/citygml/citygml.h" NAMES citygml )	

	if( LIBCITYGML_ROOT_DIR )
		set( LIBCITYGML_INCLUDE_DIRS "${LIBCITYGML_ROOT_DIR}/include" )		
		set( LIBCITYGML_LIBRARY_DIRS "${LIBCITYGML_ROOT_DIR}/lib" "${LIBCITYGML_ROOT_DIR}/bin" )	
		set( LIBCITYGML_LIBRARIES debug citygmld optimized citygml )
	endif( LIBCITYGML_ROOT_DIR )
	
endif( NOT VLIBCITYGML_FOUND )

find_package_handle_standard_args( VLIBCITYGML "libcitygml could not be found" LIBCITYGML_ROOT_DIR )
