include( FindPackageHandleStandardArgs )
include( VistaFindUtils )

if( NOT VGDAL_FOUND )

    vista_find_package_root( gdal "include/gcore/gdal.h" )
	
    if( GDAL_ROOT_DIR )
	
        set( GDAL_INCLUDE_DIRS "${GDAL_ROOT_DIR}/include" )
        set( GDAL_LIBRARY_DIRS "${GDAL_ROOT_DIR}/lib" "${GDAL_ROOT_DIR}/bin" )
		set( GDAL_LIBRARIES gdal_i )
		
	else( )
	
		message( "Could not find GDAL root directory: ${GDAL_ROOT_DIR}" )
	
    endif( )

endif( NOT VGDAL_FOUND )

find_package_handle_standard_args( Vgdal "GDAL could not be found" GDAL_ROOT_DIR )
