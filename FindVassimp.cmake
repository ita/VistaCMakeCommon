include( FindPackageHandleStandardArgs )
include( VistaFindUtils )

if( NOT VASSIMP_FOUND )
	vista_find_package_root( ASSIMP "include/assimp/Importer.hpp" NAMES ASSIMP )	

	if( ASSIMP_ROOT_DIR )
		set( ASSIMP_INCLUDE_DIRS "${ASSIMP_ROOT_DIR}/include" )		
		set( ASSIMP_LIBRARY_DIRS "${ASSIMP_ROOT_DIR}/lib" "${ASSIMP_ROOT_DIR}/bin" )	
		set( ASSIMP_LIBRARIES assimp-vc120-mt )
	endif( )
endif( )

find_package_handle_standard_args( VASSIMP "ASSIMP could not be found" ASSIMP_ROOT_DIR )
